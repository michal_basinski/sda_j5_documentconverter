package sda.documents.writers.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.type.TypeFactory;
import sda.documents.exceptions.FileWriterException;
import sda.documents.writers.IFileWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class JSONAlternativeFileWriter implements IFileWriter {
    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        try {
            FileWriter fileWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            ObjectWriter objectWriter = new ObjectMapper().writerWithType(TypeFactory.collectionType(List.class, Map.class));
            String jsonToSave = objectWriter.writeValueAsString(data);
            bufferedWriter.write(jsonToSave);
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e);
        }

    }
}
