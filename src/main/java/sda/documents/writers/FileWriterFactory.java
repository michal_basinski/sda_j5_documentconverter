package sda.documents.writers;

import sda.documents.commons.SupportedFileExtensions;
import sda.documents.writers.impl.CSVFileWriter;
import sda.documents.writers.impl.ExcelFileWriter;
import sda.documents.writers.impl.JSONAlternativeFileWriter;
import sda.documents.writers.impl.XMLFileWriter;

public class FileWriterFactory {

    public IFileWriter produce(String filePath) {
        IFileWriter result = null;
        if (filePath.endsWith(SupportedFileExtensions.CSV)) {
            result = new CSVFileWriter();
        }
        if (filePath.endsWith(SupportedFileExtensions.JSON)) {
            result = new JSONAlternativeFileWriter();
        }
        if (filePath.endsWith(SupportedFileExtensions.XML)) {
            result = new XMLFileWriter();
        }
        if (filePath.endsWith(SupportedFileExtensions.EXCEL)) {
            result = new ExcelFileWriter();
        }
        return result;
    }
}
