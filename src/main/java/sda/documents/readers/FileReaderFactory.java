package sda.documents.readers;

import sda.documents.commons.SupportedFileExtensions;
import sda.documents.readers.impl.CSVFileReader;
import sda.documents.readers.impl.ExcelFileReader;
import sda.documents.readers.impl.JSONAlternativeFileReader;
import sda.documents.readers.impl.XMLFileReader;

public class FileReaderFactory {

    public IFileReader produce(String filePath) {
        IFileReader result = null;
        if (filePath.endsWith(SupportedFileExtensions.CSV)) {
            result = new CSVFileReader();
        }
        if (filePath.endsWith(SupportedFileExtensions.JSON)) {
            result = new JSONAlternativeFileReader();
        }
        if (filePath.endsWith(SupportedFileExtensions.XML)) {
            result= new XMLFileReader();
        }
        if (filePath.endsWith(SupportedFileExtensions.EXCEL)) {
            result = new ExcelFileReader();
        }
        return result;
    }
}
