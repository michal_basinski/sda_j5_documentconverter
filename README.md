# README #

Simple file converter. Currently supports CSV, JSON, XML and XLSX files.

Supported XMLs:
<root>
	<element>
	...
	</element>
	<element>
	...
	</element>
</root>

Supported JSONs:
[{
		"node1": "value1",
		"node2": "value2"
	},
	{
		"node1": "value3",
		"node2": "value4"
	}
]

